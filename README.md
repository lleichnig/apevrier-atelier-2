# Atelier 2: Git Repository creation

## Introduction
This repository is an exercise aiming at using git

## Gitlab Server documentation
Gitlab provides repository management features, as well as tools meant to simplify the development workflow

## Gitlab Features
A complete comparison of Gitlab editions is available at:
* https://about.gitlab.com/pricing/gitlab-com/feature-comparison/ (Saas)
* https://about.gitlab.com/pricing/self-managed/feature-comparison/ (On Premise)

